import asyncio

from dotenv import load_dotenv
from hypercorn import Config
from hypercorn.asyncio import serve


def run_api() -> None:
    from src.api.main import app

    config = Config()
    config.bind = ["0.0.0.0:6549"]
    asyncio.run(serve(app, config))


def main() -> None:
    found_env: bool = load_dotenv()
    print(f"{'Found .env' if found_env else 'Couldnt find .env'}")
    run_api()


if __name__ == "__main__":
    main()
