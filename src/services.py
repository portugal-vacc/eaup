from datetime import datetime
from typing import Optional

import requests


def faa_notams(locations: str):
    link = f"https://www.notams.faa.gov/dinsQueryWeb/queryRetrievalMapAction.do?reportType=Raw&retrieveLocId={locations}&actionType=notamRetrievalByICAOs"
    return requests.get(link).text


def get_dfs_aup(start_datetime: datetime, end_datetime: datetime) -> str:
    """Gets data from dfs aup endpoint in plain text"""
    form_data = {
        "id": "",
        "MAP": "OSM",
        "DATE_BEGIN": start_datetime.strftime("%G-%m-%d"),
        "TIME_BEGIN": start_datetime.strftime("%H:%M"),
        "DATE_END": end_datetime.strftime("%G-%m-%d"),
        "TIME_END": end_datetime.strftime("%H:%M"),
        "LOWER": "GND",
        "UPPER": "UNL",
    }
    response = requests.post(
        "https://ais.dfs.de/pilotservice/service/aup/ajax/aup_briefing.jsp",
        data=form_data,
    )
    response.raise_for_status()
    return response.text


def get_sparx_aup() -> list[Optional[dict]]:
    response = requests.get("https://sparx.virtualiac.com/api/airspace/notices")
    response.raise_for_status()
    return response.json()
