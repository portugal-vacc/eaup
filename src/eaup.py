from datetime import datetime, time
import re
from typing import List, Optional, Tuple, Union
from pydantic import BaseModel, Field, field_validator

from src.selenium import web_latest_area_data


class EaupArea(BaseModel):
    name: str
    minimum_fl: Union[int, str] = Field(ge=0, lt=1000)
    maximum_fl: Union[int, str] = Field(lt=1000)
    start_time: time
    end_time: time
    remark: Optional[str] = "AUP"

    @field_validator("minimum_fl", "maximum_fl", mode="before")
    @classmethod
    def validate_flight_level(cls, v: str) -> int:
        return int(v)


class EaupInfo(BaseModel):
    type: str
    valid_wef: datetime
    valid_til: datetime
    released_on: datetime


class Eaup(BaseModel):
    notice_info: EaupInfo
    areas: list[EaupArea]


def merge_to_eaup(info: EaupInfo, area_list: List[EaupArea]) -> Eaup:
    return Eaup(notice_info=info, areas=area_list)


def parse_area(data: Tuple[str]) -> EaupArea:
    return EaupArea(
        name=data[0],
        minimum_fl=data[1],
        maximum_fl=data[2],
        start_time=time.fromisoformat(data[3]),
        end_time=time.fromisoformat(data[4]),
        remark="AUP",
    )


def regex_data(text: str) -> List[Tuple[str]]:
    """Match ECNPZ3     095 660 12:00 06:00
    Group 1 ECNPZ3
    Group 2 095
    Group 3 660
    Group 4 12:00
    Group 5 06:00"""
    return re.findall(
        r"(\S{2,})(?:\s{5}|\s{3}CIVILIAN\sBUFFER\s|\s+A\s)(\S{3})\s{1,10}(\S{3})\s{1,10}(\d{2}:\d{2})\s{1,10}(\d{2}:\d{2})",
        text,
    )


def area_list_all_data(raw_data: str) -> List[EaupArea]:
    data_list: List[Tuple[str]] = regex_data(raw_data)
    return [parse_area(area) for area in data_list]


def parse_info(data: dict) -> EaupInfo:
    return EaupInfo(
        type=data["type"],
        valid_wef=datetime.strptime(data["valid_wef"], "%d/%m/%Y %H:%M"),
        valid_til=datetime.strptime(data["valid_til"], "%d/%m/%Y %H:%M"),
        released_on=datetime.strptime(data["released_on"], "%d/%m/%Y %H:%M"),
    )


def regex_info(text: str) -> dict:
    """Match	Type EUUP
        Valid WEF 14/03/2022 13:00
        Valid TIL 15/03/2022 06:00
        Released On 14/03/2022 13:20
    Group 1 EUUP
    Group 2 14/03/2022 13:00
    Group 3 15/03/2022 06:00
    Group 4 14/03/2022 13:20
    """
    return re.search(
        r"Type (?P<type>\S{4})\nValid WEF (?P<valid_wef>\d{2}/\d{2}/\d{4} \d{2}:\d{2})\nValid TIL (?P<valid_til>\d{"
        r"2}/\d{2}/\d{4} \d{2}:\d{2})\nReleased On (?P<released_on>\d{2}/\d{2}/\d{4} \d{2}:\d{2})",
        text,
    ).groupdict()


def notice_info_data(raw_info: str) -> EaupInfo:
    info_dict: dict = regex_info(raw_info)
    return parse_info(info_dict)


def get_eaup() -> Eaup:
    data: dict[str, str] = web_latest_area_data()
    eaup_info: EaupInfo = notice_info_data(raw_info=data["info"])
    eaup_areas: list[EaupArea] = area_list_all_data(raw_data=data["data"])
    return merge_to_eaup(info=eaup_info, area_list=eaup_areas)


def area_list_country_data(
    eaup_area_list: List[EaupArea], country_code: str = "LP"
) -> List[EaupArea]:
    return [
        area for area in eaup_area_list if area.name.startswith(country_code.upper())
    ]
