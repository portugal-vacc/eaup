import re
from datetime import datetime, timedelta
from typing import List, Optional

from bs4 import BeautifulSoup, element
from pydantic import BaseModel, field_validator, field_serializer
from requests import HTTPError
from src.eaup import EaupArea

from src.services import get_dfs_aup


class BriefingHeader(BaseModel):
    prepared_on: datetime
    valid_from: datetime
    valid_until: datetime


def parse_briefingheadercontainer(table: element.Tag) -> BriefingHeader:
    datetimes = [
        str(time["datetime"]).replace("Z", "") for time in table.find_all("time")
    ]
    return BriefingHeader(
        prepared_on=datetime.fromisoformat(datetimes[0]),
        valid_from=datetime.fromisoformat(datetimes[1]),
        valid_until=datetime.fromisoformat(datetimes[2]),
    )


class Airspace(BaseModel):
    name: str
    polygon: List[str]
    lower_limit: int
    upper_limit: int
    start_datetime: datetime
    end_datetime: datetime

    @field_serializer("start_datetime")
    def serialize_start_datetime(self, dt: datetime, _info):
        return dt.isoformat()

    @field_serializer("end_datetime")
    def serialize_end_datetime(self, dt: datetime, _info):
        return dt.isoformat()

    @field_validator("lower_limit", mode="before")
    @classmethod
    def serialize_lower_limit(cls, v: str) -> int:
        if v in ["MSL", "GND"]:
            return 0
        if isinstance(v, int):
            return v
        return int(v[1:])

    @field_validator("upper_limit", mode="before")
    @classmethod
    def serialize_upper_limit(cls, v: str) -> int:
        if v == "UNL":
            return 999
        if isinstance(v, int):
            return v
        return int(v[1:])


def parse_airspace(table: element.Tag):
    validity = table.tbody.find_all("tr", "validity")
    for area_info in validity:
        datetimes = [
            str(time["datetime"]).replace("Z", "") for time in table.find_all("time")
        ]

        match = re.search(
            r"(?P<lower>\w*) to Upper Limit (?P<upper>\w*)", area_info.text
        )

        yield Airspace(
            name=table.thead.tr.th.text,
            polygon=table["data-polygon"].split("-"),
            lower_limit=match.group("lower"),
            upper_limit=match.group("upper"),
            start_datetime=datetime.fromisoformat(datetimes[0]),
            end_datetime=datetime.fromisoformat(datetimes[1]),
        )


class Dfs_Aup(BaseModel):
    header: BriefingHeader
    areas: List[Optional[Airspace]]


def parse_html(html_text: str) -> Dfs_Aup:
    soup = BeautifulSoup(html_text, "html.parser")

    header_html = soup.find("table", "briefingheadercontainer")
    header = parse_briefingheadercontainer(header_html)

    areas = list()
    for table in soup.find_all("table", "airspace"):
        areas.extend([area for area in parse_airspace(table)])
    return Dfs_Aup(header=header, areas=areas)


def get_dfs_areas(
    start_datetime: datetime = None, end_datetime: datetime = None
) -> Dfs_Aup:
    start = start_datetime if start_datetime else datetime.now()
    end = end_datetime if end_datetime else datetime.now() + timedelta(days=1)
    try:
        aup_html = get_dfs_aup(start_datetime=start, end_datetime=end)
    except HTTPError as http_error:
        raise Exception(f"Something gone wrong getting DFS data {http_error}")
    return parse_html(aup_html)


def parse_dfs_areas_to_eaup(dfs_aup: Dfs_Aup) -> list[EaupArea]:
    eaup_areas = list()
    for area in dfs_aup.areas:
        eaup_areas.append(
            EaupArea(
                name=area.name,
                minimum_fl=(
                    0 if area.lower_limit in ["GND", "MSL"] else area.lower_limit[1:]
                ),
                maximum_fl=area.upper_limit[1:],
                start_time=area.start_datetime.time(),
                end_time=area.end_datetime.time(),
                remark="DFS",
            )
        )
    return eaup_areas
