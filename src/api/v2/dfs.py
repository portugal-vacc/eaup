from datetime import datetime

from fastapi import APIRouter

from src.dfs import Dfs_Aup, get_dfs_areas

dfs_api = APIRouter(prefix="/dfs")


@dfs_api.get("/areas/", response_model=Dfs_Aup)
def get_dfs_data(start_datetime: datetime = None, end_datetime: datetime = None):
    return get_dfs_areas(start_datetime, end_datetime)
