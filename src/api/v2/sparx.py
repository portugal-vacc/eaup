from datetime import datetime

from fastapi import APIRouter
from src.eaup import EaupArea

from src.services import get_sparx_aup

sparx_api = APIRouter(prefix="/sparx")

from pydantic import BaseModel


class SparxArea(BaseModel):
    id: int
    airspace_name: str
    min_alt: str
    max_alt: str
    time_from: str
    time_till: str

    def to_aup(self):
        return EaupArea(
            name=f"EI{self.airspace_name}",
            minimum_fl=self.min_alt,
            maximum_fl=self.max_alt,
            start_time=datetime.fromisoformat(self.time_from).time(),
            end_time=datetime.fromisoformat(self.time_till).time(),
            remark="Sparx",
        )


@sparx_api.get("/areas/")
def get_sparx_data():
    return [SparxArea(**area).to_aup() for area in get_sparx_aup()]
