from fastapi import APIRouter

from src.selenium import web_latest_area_data
from src.storage import eaup_data, read_cache

eurocontrol_api = APIRouter(prefix="/eurocontrol")


@eurocontrol_api.get("/webscrape/")
def get_webscrape_data():
    return web_latest_area_data()


@eurocontrol_api.get("/cache")
def cache():
    return read_cache()


@eurocontrol_api.post("/cache/update")
def cache_update():
    return eaup_data(force=True)
