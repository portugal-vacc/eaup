from fastapi import FastAPI, APIRouter
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse

from src.api.eurocontrol import eurocontrol_api
from src.api.v1.main import v1_api
from src.api.v2.main import v2_api
from src.api.v3.main import v3_api

description = """
Data Source:  <a href="https://www.public.nm.eurocontrol.int/PUBPORTAL/gateway/spec/">Eurocontrol</a>

Notams Source: <a href="https://www.notams.faa.gov/dinsQueryWeb/">FAA</a>

Special thanks to:
<ul>
<li>Ricardo - 1110850</li>
<li>Matisse - 1385143</li>
</ul>

<h1>For Flight Simulation Only!</h1>
"""

app = FastAPI(
    title="EuroScope European Airspace Use Plan Service API",
    docs_url="/api/docs",
    description=description,
    contact={
        "name": "Rodrigo Simões - 1438868",
        "email": "rodrigo.simoes@portugal-vacc.org",
    },
    license_info={
        "name": "GNU General Public License v3.0",
        "url": "https://www.gnu.org/licenses/gpl-3.0.en.html",
    },
    openapi_url="/api/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def redirect_to_docs():
    return RedirectResponse("/api/docs")


api = APIRouter(prefix="/api")

api.include_router(v3_api)
api.include_router(v2_api)
api.include_router(v1_api)
api.include_router(eurocontrol_api)

app.include_router(api)
