from datetime import datetime, time
from src.api.v3.pools import get_pool, get_pools
from src.api.v3.schemas import Area, Header, Publication
from src.dfs import Dfs_Aup
from src.eaup import Eaup
from src.sparx import Sparx


def convert_sparx(sparx_data: Sparx) -> Publication:
    areas = [
        Area(
            name=area.airspace_name,
            source="sparx",
            lower_limit=area.min_alt,
            upper_limit=area.max_alt,
            start_datetime=area.time_from,
            end_datetime=area.time_till,
            remark=f"Sts {area.status} | Updated {area.updated_at}",
        )
        for area in sparx_data.areas
    ]
    return Publication(header=Header(source="sparx"), areas=areas)


def convert_dfs(dfs_data: Dfs_Aup) -> Publication:
    areas = [
        Area(
            name=area.name,
            source="dfs",
            lower_limit=area.lower_limit,
            upper_limit=area.upper_limit,
            start_datetime=area.start_datetime,
            end_datetime=area.end_datetime,
        )
        for area in dfs_data.areas
    ]
    return Publication(
        header=Header(
            source="dfs",
            valid_from=dfs_data.header.valid_from,
            valid_until=dfs_data.header.valid_until,
            released_on=dfs_data.header.prepared_on,
        ),
        areas=areas,
    )


def eaup_area_start_time_to_datetime(
    notice_from: datetime, notice_until: datetime, area_start_time: time
) -> datetime:
    return (
        notice_from.replace(hour=area_start_time.hour, minute=area_start_time.minute)
        if area_start_time >= notice_from.time()
        else notice_until.replace(
            hour=area_start_time.hour, minute=area_start_time.minute
        )
    )


def eaup_area_end_time_to_datetime(
    notice_from: datetime, notice_until: datetime, area_end_time: time
) -> datetime:
    return (
        notice_until.replace(hour=area_end_time.hour, minute=area_end_time.minute)
        if area_end_time <= notice_until.time()
        else notice_from.replace(hour=area_end_time.hour, minute=area_end_time.minute)
    )


def convert_eaup(eaup: Eaup) -> Publication:
    areas = [
        Area(
            name=area.name,
            source="eaup",
            lower_limit=area.minimum_fl,
            upper_limit=area.maximum_fl,
            start_datetime=eaup_area_start_time_to_datetime(
                eaup.notice_info.valid_wef, eaup.notice_info.valid_til, area.start_time
            ),
            end_datetime=eaup_area_end_time_to_datetime(
                eaup.notice_info.valid_wef, eaup.notice_info.valid_til, area.end_time
            ),
        )
        for area in eaup.areas
    ]
    return Publication(
        header=Header(
            source="eaup",
            type=eaup.notice_info.type,
            valid_from=eaup.notice_info.valid_wef,
            valid_until=eaup.notice_info.valid_til,
            released_on=eaup.notice_info.released_on,
        ),
        areas=areas,
    )


def areas() -> list[Area]:
    areas = []
    for publication in [
        Publication.parse_obj(get_pool(provider)[0]) for provider in get_pools()
    ]:
        areas.extend(publication.areas)
    return sorted(areas, key=lambda area: area.name)
