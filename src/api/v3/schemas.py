from typing import Optional
from pydantic import BaseModel, Field, field_serializer, field_validator
from datetime import datetime
import stringcase


class Area(BaseModel):
    name: str
    source: str
    lower_limit: int = Field(ge=0, lt=1000)
    upper_limit: int = Field(lt=1000)
    start_datetime: datetime
    end_datetime: datetime
    remark: Optional[str] = ""

    @field_validator("name", mode="before")
    @classmethod
    def serialize_name(cls, v: str) -> str:
        return stringcase.uppercase(stringcase.alphanumcase(v))

    @field_serializer("start_datetime")
    def serialize_start_datetime(self, dt: datetime, _info):
        return dt.isoformat()

    @field_serializer("end_datetime")
    def serialize_end_datetime(self, dt: datetime, _info):
        return dt.isoformat()


class Header(BaseModel):
    source: str
    type: Optional[str] = None
    valid_from: Optional[datetime] = datetime.utcnow()
    valid_until: Optional[datetime] = None
    released_on: datetime = datetime.utcnow()

    @field_serializer("valid_from")
    def serialize_valid_from(self, dt: datetime, _info):
        if not dt:
            return None
        return dt.isoformat()

    @field_serializer("valid_until")
    def serialize_valid_until(self, dt: datetime, _info):
        if not dt:
            return None
        return dt.isoformat()

    @field_serializer("released_on")
    def serialize_released_on(self, dt: datetime, _info):
        if not dt:
            return None
        return dt.isoformat()


class Publication(BaseModel):
    header: Header
    areas: list[Optional[Area]] = []
