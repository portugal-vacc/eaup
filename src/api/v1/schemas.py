from datetime import time, date
from math import floor, ceil
from typing import Union, List

from pydantic import BaseModel

from src.eaup import EaupArea, EaupInfo


class TopSkyArea(BaseModel):
    name: str
    start_date: date
    end_date: date
    week_days: int
    start_time: time
    end_time: time
    lower: int
    upper: int
    user_text: str


TOP_CONTROLLED_LEVEL = 660
BOTTOM_CONTROLLED_LEVEL = 5
TRANSITION_ALTITUDE = 4000


class TopSkyParser(BaseModel):
    notice_info: EaupInfo
    area: EaupArea

    @property
    def name(self) -> str:
        return self.area.name

    @property
    def start_date(self) -> date:
        return self.notice_info.valid_wef.date()

    @property
    def end_date(self) -> date:
        if self.start_time > self.end_time:
            return self.notice_info.valid_til.date()
        return self.notice_info.valid_wef.date()

    @property
    def week_days(self) -> int:
        return 0

    @property
    def start_time(self) -> time:
        return self.area.start_time

    @property
    def end_time(self) -> time:
        return self.area.end_time

    @property
    def lower(self) -> int:
        return self.area.minimum_fl * 100

    @property
    def upper(self) -> int:
        return self.area.maximum_fl * 100

    @staticmethod
    def ceil_flight_level(flight_level: int) -> int:
        return ceil(flight_level / 10) * 10

    @staticmethod
    def floor_flight_level(flight_level: int) -> int:
        return floor(flight_level / 10) * 10

    @property
    def top_required_separation(self) -> int:
        return 20 if self.area.maximum_fl >= 410 else 10

    @property
    def bottom_required_separation(self) -> int:
        return 20 if self.area.minimum_fl >= 410 else 10

    @property
    def bottom_area_safe_level(self) -> int:
        if self.is_above_transition_altitude(self.area.minimum_fl):
            return (
                self.floor_flight_level(self.area.minimum_fl)
                - self.bottom_required_separation
            )
        return self.area.minimum_fl - self.bottom_required_separation

    @property
    def top_area_safe_level(self) -> int:
        return (
            self.ceil_flight_level(self.area.maximum_fl) + self.top_required_separation
        )

    def calculate_max(self) -> Union[str, None]:
        if (self.top_area_safe_level > TOP_CONTROLLED_LEVEL) and (
            self.bottom_area_safe_level >= BOTTOM_CONTROLLED_LEVEL
        ):
            return f"{self.formatter(self.bottom_area_safe_level)}MAX"
        return None

    @staticmethod
    def is_above_transition_altitude(level: int) -> bool:
        transition_altitude_two_digit = TRANSITION_ALTITUDE / 100
        if level > transition_altitude_two_digit:
            return True
        return False

    @staticmethod
    def format_for_altitude(altitude: int) -> str:
        altitude_as_string = str(altitude * 100)
        string_len = len(altitude_as_string)
        if string_len < 4:
            for _ in range(4 - string_len):
                altitude_as_string = "0" + altitude_as_string
        return altitude_as_string

    @staticmethod
    def format_for_level(level: int) -> str:
        level_as_string = str(level)
        string_len = len(level_as_string)
        if string_len < 3:
            for _ in range(3 - string_len):
                level_as_string = "0" + level_as_string
        return level_as_string

    def formatter(self, level: int) -> str:
        return (
            f"SFL{self.format_for_level(level)}"
            if self.is_above_transition_altitude(level)
            else f"SFA{self.format_for_altitude(level)}"
        )

    @property
    def safe_level_text(self) -> str:
        if self.calculate_max():
            return self.calculate_max()

        if (
            self.top_area_safe_level > TOP_CONTROLLED_LEVEL
            and self.bottom_area_safe_level < BOTTOM_CONTROLLED_LEVEL
        ):
            return "UNL"

        return self.formatter(self.top_area_safe_level)

    @property
    def user_text(self) -> str:
        return self.safe_level_text

    def output_area(self) -> TopSkyArea:
        return TopSkyArea(
            name=self.name,
            start_date=self.start_date,
            end_date=self.end_date,
            week_days=self.week_days,
            start_time=self.start_time,
            end_time=self.end_time,
            lower=self.lower,
            upper=self.upper,
            user_text=self.user_text,
        )


class TopSky(BaseModel):
    notice_info: EaupInfo
    areas: List[TopSkyArea]


def parse_top_sky_areas(
    notice_info: EaupInfo, areas: List[EaupArea]
) -> List[TopSkyArea]:
    # print(type(notice_info), type(areas[0]))
    return [
        TopSkyParser(notice_info=notice_info, area=area).output_area() for area in areas
    ]
