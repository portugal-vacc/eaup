from fastapi import APIRouter
from starlette.responses import HTMLResponse

from src.country_codes import COUNTRY_CODES
from src.eaup import area_list_country_data
from src.api.v1.schemas import TopSky, parse_top_sky_areas
from src.services import faa_notams
from src.storage import eaup_data
from src.api.v1.helpers import top_sky_area_manual_format, top_sky_area_notam_format

topsky_api = APIRouter(prefix="/topsky")


@topsky_api.get("/areas/", response_model=TopSky, include_in_schema=False)
def get_all_areas():
    eaup = eaup_data()
    top_sky_areas = parse_top_sky_areas(notice_info=eaup.notice_info, areas=eaup.areas)
    return TopSky(notice_info=eaup.notice_info, areas=top_sky_areas)


@topsky_api.get(
    "/areas/{country_code}/", response_model=TopSky, include_in_schema=False
)
def get_country_areas(country_code: COUNTRY_CODES):
    eaup = eaup_data()
    eaup_country_areas = area_list_country_data(
        eaup_area_list=eaup.areas, country_code=country_code
    )
    top_sky_areas = parse_top_sky_areas(
        notice_info=eaup.notice_info, areas=eaup_country_areas
    )
    return TopSky(notice_info=eaup.notice_info, areas=top_sky_areas)


@topsky_api.get(
    "/areas/{country_code}/textfile/",
    response_class=HTMLResponse,
    include_in_schema=False,
)
def get_country_areas_for_textfile_format(country_code: COUNTRY_CODES):
    eaup = eaup_data()
    eaup_country_areas = area_list_country_data(
        eaup_area_list=eaup.areas, country_code=country_code
    )
    top_sky_areas = parse_top_sky_areas(
        notice_info=eaup.notice_info, areas=eaup_country_areas
    )
    areas_as_string = (top_sky_area_manual_format(area) for area in top_sky_areas)
    return "<br>".join(areas_as_string)


@topsky_api.get(
    "/areas/{country_code}/notam/", response_class=HTMLResponse, deprecated=True
)
def get_country_areas_for_notam_format(country_code: COUNTRY_CODES, loc: str = None):
    eaup = eaup_data()
    notams_faa = faa_notams(locations=loc) if loc else ""
    eaup_country_areas = area_list_country_data(
        eaup_area_list=eaup.areas, country_code=country_code
    )
    top_sky_areas = parse_top_sky_areas(
        notice_info=eaup.notice_info, areas=eaup_country_areas
    )
    areas_as_notam = (top_sky_area_notam_format(area) for area in top_sky_areas)
    eaup_as_notam = "<br>".join(areas_as_notam)
    return eaup_as_notam + notams_faa
