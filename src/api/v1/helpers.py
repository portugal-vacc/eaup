from src.api.v1.schemas import TopSkyArea


def top_sky_area_manual_format(area: TopSkyArea) -> str:
    return (
        f"{area.name[2:]}:{area.start_date.strftime('%y%m%d')}:"
        f"{area.end_date.strftime('%y%m%d')}:{area.week_days}:"
        f"{area.start_time.strftime('%H%M')}:{area.end_time.strftime('%H%M')}:"
        f"{area.lower}:{area.upper}:{area.user_text}"
    )


def top_sky_area_notam_format(area: TopSkyArea) -> str:
    return (
        f"<div><PRE>{area.user_text} NOTAMN \n"
        f"Q) LPPC/QRALW/IV/NBO/W /{round(area.lower / 100)}/{round(area.upper / 100)}/4058N00705W041 \n"
        f"A) LPPC B) {area.start_date.strftime('%y%m%d')}{area.start_time.strftime('%H%M')} C) {area.end_date.strftime('%y%m%d')}{area.end_time.strftime('%H%M')} \n"
        f"D) {area.start_time.strftime('%H%M')}-{area.end_time.strftime('%H%M')} \n"
        f"E) AIRSPACE RESERVATION WILL TAKE PLACE, AREA: {area.name} \n"
        f"F) FL{round(area.lower / 100)} G) FL{round(area.upper / 100)} \n"
        f"</PRE></div>\n"
    )
