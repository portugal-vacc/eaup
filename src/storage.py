import os
from datetime import datetime, timedelta
from pathlib import Path
from typing import Dict, List

from pydantic import BaseModel

from src.eaup import notice_info_data
from src.eaup import Eaup, EaupArea, EaupInfo, area_list_all_data, merge_to_eaup
from src.selenium import web_latest_area_data

FILE_CACHE_PATH = Path(os.getenv("FILE_CACHE_PATH", "./eaup_cache.json"))

if not FILE_CACHE_PATH.parent.exists():
    os.mkdir(FILE_CACHE_PATH.parent)


class StorageProtocol(BaseModel):
    ttl_minutes: int = 3600
    retrieved_at: datetime = None
    eaup: Eaup = None

    @property
    def is_outdated(self):
        return (
            True
            if datetime.now() - self.retrieved_at > timedelta(minutes=self.ttl_minutes)
            else False
        )


def read_cache() -> None | str:
    try:
        with open(FILE_CACHE_PATH, "r") as file:
            return file.read()
    except FileNotFoundError:
        return None


def write_cache(data) -> None:
    FILE_CACHE_PATH.touch(exist_ok=True)
    with open(FILE_CACHE_PATH, "w+") as file:
        file.write(data)


def get_eaup(ttl_minutes: int) -> StorageProtocol:
    data: Dict[str, str] = web_latest_area_data()
    eaup_info: EaupInfo = notice_info_data(raw_info=data["info"])
    eaup_areas: List[EaupArea] = area_list_all_data(raw_data=data["data"])
    return StorageProtocol(
        ttl_minutes=ttl_minutes,
        retrieved_at=datetime.now(),
        eaup=merge_to_eaup(info=eaup_info, area_list=eaup_areas),
    )


def eaup_data(force: bool = False) -> Eaup:
    ttl_minutes = os.getenv("TIME_TO_LIVE_MINUTES", 30)
    cache = read_cache()
    if force or not cache:
        protocol = get_eaup(ttl_minutes=ttl_minutes)
        write_cache(protocol.json())
        return protocol.eaup
    protocol = StorageProtocol.parse_raw(cache)
    if protocol.is_outdated:
        protocol = get_eaup(ttl_minutes=ttl_minutes)
        write_cache(protocol.json())
    return protocol.eaup
