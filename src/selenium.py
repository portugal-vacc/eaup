import os
from contextlib import contextmanager
from typing import Dict

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from src.logging import get_logger

selenium_server = os.environ["SELENIUM_SERVER"]  # http://selenium:4444/wd/hub

logger = get_logger(__name__)


@contextmanager
def web():
    options = webdriver.ChromeOptions()
    # options.add_argument('--headless')
    options.add_argument("--disable-blink-features=AutomationControlled")
    driver = webdriver.Remote(f"{selenium_server}/wd/hub", options=options)
    logger.debug("YieldDriver")
    yield driver
    driver.quit()
    logger.debug("DriverQuitted")


def web_latest_area_data() -> Dict[str, str]:
    with web() as driver:
        driver.get("https://www.public.nm.eurocontrol.int/PUBPORTAL/gateway/spec/")
        assert "NOP Network Operations Portal" in driver.title
        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "active_eaup_highlight"))
        )
        elements = driver.find_elements(
            By.XPATH,
            "//tr[@class='active_eaup_highlight']//div[@class='eurocontrol_gwt_ext_linkEnabled']",
        )
        elements[0].click()
        driver.switch_to.window(driver.window_handles[1])
        WebDriverWait(driver, 10).until(
            EC.none_of(
                EC.presence_of_element_located(
                    (
                        By.CLASS_NAME,
                        "eurocontrol_gwt_ext_glassPanelServer eurocontrol_gwt_ext_glassPanel_zIndex_panel_5",
                    )
                )
            )
        )
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.ID, "gwt-debug-EaupDetailsTabPanel-bar-tab1")
            )
        )
        driver.find_element(By.ID, "gwt-debug-EaupDetailsTabPanel-bar-tab1").click()
        validity = driver.find_element(
            By.XPATH,
            "//table[@class='portal_fullWidthStyle portal_emphasised eurocontrol_gwt_ext_fullWidthStyle']//tbody[1]",
        )
        table = driver.find_element(By.ID, "gwt-debug-EaupDetailsTabPanel-bottom")
        logger.info(f"Webscraped task completed! Validity: {validity.text}")
        return {"info": validity.text, "data": table.text}
