# syntax=docker/dockerfile:1
FROM python:3.10-slim

WORKDIR /code

RUN pip install --upgrade pip

ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY . .

EXPOSE 6549

CMD [ "python", "run.py"]
